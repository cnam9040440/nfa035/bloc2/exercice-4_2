package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Set;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 * TODO
 */
public class WalletSerializerDirectAccessImpl
        extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia>
 {

     public WalletSerializerDirectAccessImpl(Set<DigitalBadge> walletMetadata) {
         super();
     }

     /**
     * {@inheritDoc}
     *
     * @param digitalBadge
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(DigitalBadge digitalBadge) throws IOException {
        return new FileInputStream(digitalBadge.getBadge());
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(WalletFrameMedia media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(),true,0,null));
    }


    @Override
    /**
     * {@inheritDoc}
     */
    public final void serialize(DigitalBadge digitalBadge, WalletFrameMedia media) throws IOException {
        long size = Files.size(digitalBadge.getBadge().toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            long numberOfLines = media.getNumberOfLines();
            PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
            SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDateStrBegin = dmyFormat.format(digitalBadge.getBegin());
            String formattedDateStrEnd = dmyFormat.format(digitalBadge.getBegin());
           // writer.printf("%1$d;", size);
           // writer.printf("%1$d;%2$d;%3$d;%4$s;%5$s;%6$s;", numberOfLines + 2, media.getChannel().getFilePointer(), size, digitalBadge.getSerial(), formattedDateStrBegin, formattedDateStrEnd);
            if(media.getNumberOfLines() ==0){
                writer.printf("%1$d;%2$s;%3$s;%4$s;", size, digitalBadge.getSerial(), formattedDateStrBegin, formattedDateStrEnd);
            }else{
                writer.printf(";%1$d;%2$s;%3$s;%4$s;", size, digitalBadge.getSerial(), formattedDateStrBegin, formattedDateStrEnd);
            }

            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(digitalBadge).transferTo(eos);
                eos.flush();
            }
            writer.printf("\n");
            writer.printf("%1$d;%2$d", numberOfLines + 2, media.getChannel().getFilePointer());
        }
        media.incrementLines();
    }
    /** public final void serialize(DigitalBadge digitalBadge, WalletFrameMedia media) throws IOException {
         long size = Files.size(digitalBadge.getBadge().toPath());
         try(OutputStream os = media.getEncodedImageOutput()) {
             long numberOfLines = media.getNumberOfLines();
             PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
             writer.printf("%1$d;", size);
             try(OutputStream eos = getSerializingStream(media)) {
                 getSourceInputStream(digitalBadge).transferTo(eos);
                 eos.flush();
             }
             writer.printf("\n");
             writer.printf("%1$d;%2$d;",numberOfLines + 2, media.getChannel().getFilePointer());
         }
         media.incrementLines();
     }**/

}
