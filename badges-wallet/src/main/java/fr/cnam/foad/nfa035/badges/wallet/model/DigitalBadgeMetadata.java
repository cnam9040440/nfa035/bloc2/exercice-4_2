package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

/**
 * POJO model représentant les Métadonnées d'un Badge Digital
 */
public class DigitalBadgeMetadata implements Comparable{

    private int badgeId;
    private long walletPosition;
    private long imageSize;

    /**
     * Constructeur
     *
     * @param badgeId
     * @param imageSize
     */
    public DigitalBadgeMetadata(int badgeId, long walletPosition, long imageSize) {
        this.badgeId = badgeId;
        this.walletPosition = walletPosition;
        this.imageSize = imageSize;
    }

    /**
     * Getter du badgeId
     * @return l'id du badge
     */
    public int getBadgeId() {
        return badgeId;
    }

    /**
     * Setter du badgeId
     * @param badgeId
     */
    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    /**
     * Getter de la taille de l'image
     * @return long la taille de l'image
     */
    public long getImageSize() {
        return imageSize;
    }

    /**
     * Setter de l'imageSize
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    /**
     * Getter de la position du badge dans le wallet
     * @return long la position du badge dans le wallet
     */
    public long getWalletPosition() {
        return walletPosition;
    }

    /**
     * Setter de la position du badge dans le wallet
     * @param walletPosition
     */
    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    /**
     * {@inheritDoc}
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return badgeId == that.badgeId && walletPosition == that.walletPosition && imageSize == that.imageSize;
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(badgeId, walletPosition, imageSize);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeId=" + badgeId +
                ", imageSize=" + imageSize +
                '}';
    }


    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * <p>The implementor must ensure {@link Integer#signum
     * signum}{@code (x.compareTo(y)) == -signum(y.compareTo(x))} for
     * all {@code x} and {@code y}.  (This implies that {@code
     * x.compareTo(y)} must throw an exception if and only if {@code
     * y.compareTo(x)} throws an exception.)
     *
     * <p>The implementor must also ensure that the relation is transitive:
     * {@code (x.compareTo(y) > 0 && y.compareTo(z) > 0)} implies
     * {@code x.compareTo(z) > 0}.
     *
     * <p>Finally, the implementor must ensure that {@code
     * x.compareTo(y)==0} implies that {@code signum(x.compareTo(z))
     * == signum(y.compareTo(z))}, for all {@code z}.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     * @apiNote It is strongly recommended, but <i>not</i> strictly required that
     * {@code (x.compareTo(y)==0) == (x.equals(y))}.  Generally speaking, any
     * class that implements the {@code Comparable} interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     */
    @Override
    public int compareTo(Object o) {
        DigitalBadgeMetadata digitalBadgeMetadata = (DigitalBadgeMetadata) o;
        if (this.getBadgeId()==((DigitalBadgeMetadata) o).getBadgeId()){
            return 0;
        } else if(this.getBadgeId()<((DigitalBadgeMetadata) o).getBadgeId()){
            return -1;
        } else {
            return 1;
        }
    }
}
